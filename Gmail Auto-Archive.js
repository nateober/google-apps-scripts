// Gmail Auto-Archive
// Auto archive items that you would like to dissapear after remaining in the inbox for a specific period of time
// Default labels include three different time delays
// * 24 Hours - This is good for newsletter and other items that aren't important after a day or so
// * 48 Hours - Good for notifications and other information that you may actually want to dig into
// * 1 week - If you haven't dealt with an email after 1 week, you probably won't deal with it

var userProperties = PropertiesService.getUserProperties();

function setUserSpreadsheet(sheetID, tab){
  return userProperties.setProperties({
    'sheet' : sheetID,
    'tab': tab
  });
}

function getUserSpreadsheet(){
  //console.log(userProperties.getProperties());
  return userProperties.getProperties();
}

function createUserSpreadsheet(){
  var newSheet = SpreadsheetApp.create("Gmail Auto-Archive Labels");
  newSheet.getSheets()[0].setName('Labels');

  let defaults = [
    ["Name",	"Time",	"Remove Label",	"Delete"],
    ["24", "24", "TRUE", "FALSE"],
    ["48", "48", "TRUE", "FALSE"],
    ["1 week", "168", "TRUE", "FALSE"]
  ];

  defaults.forEach(function(row){
    newSheet.appendRow(row)
  }) 

  setUserSpreadsheet(newSheet.getId(), 'Labels'); 
}

function Intialize() {
  return;
}

function Install() {
  createUserSpreadsheet();
  getLabels();
  ScriptApp.newTrigger("archiveMail")
           .timeBased().everyHours(1).create();
}

function Uninstall() {
  var triggers = ScriptApp.getScriptTriggers();
  triggers.forEach(function(trigger){
    ScriptApp.deleteTrigger(trigger);
  })
}

function getLabels(){
  var sheet = SpreadsheetApp.openById(getUserSpreadsheet().sheet);
  var tab = sheet.getSheetByName(getUserSpreadsheet().tab); 
  var rows = tab.getDataRange();
  var values = rows.getValues();
 
  let milliseconds = (hours) => hours*1000*60*60;
  let labels = [];

  //ditch headers
  values.shift();

    values.forEach(function (row){
      let label = {
        'name': row[0].toString().trim(), 
        'milliseconds':milliseconds(row[1]), 
        'remove': row[2], 
        'delete': row[3]
         }
    
    // If the labels in our spreadsheet don't exist in the system, we must create them
    if(GmailApp.getUserLabelByName(label.name) == null){ 
      GmailApp.createLabel(label.name);
    }
      label.obj = GmailApp.getUserLabelByName(label.name);
      labels.push(label);
    })

    return labels;
}

function archiveMail(){
  let labels = getLabels();
  let now = new Date().getTime();

  labels.forEach(function(label){
    console.log("Now processing " + label.name);
    
    var searchString = (label.remove == true) ? '(label: "' + label.name + '")' : '(label:inbox OR is:unread)(label:"' + label.name + '")';
    let threads = GmailApp.search(searchString);
    
    console.log("Search String: " + searchString + "\nGot " + threads.length + " threads to process.");

    threads.forEach(function(thread){
      var expiresOn = thread.getLastMessageDate().getTime() + label.milliseconds;
      if(expiresOn < now){
        thread.markRead();
        if(label.delete == true){
          //console.log("Deleting email with label " + label.name + " with subject: " + thread.getMessages()[0].getSubject())
          thread.moveToTrash();
        }
        else{
          thread.moveToArchive();
        }
        if(label.remove == true){
          //console.log("Removing label " + label.name + " from email with subject: " + thread.getMessages()[0].getSubject());
          thread.removeLabel(label.obj);
        }
      }
    })
  })
}

