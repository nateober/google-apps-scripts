# Google Apps Scripts Automation #


### Auto-Archive Specific Gmail Labels ###
If you're like me, there are different kinds of emails that you would like to be notified about, but that you don't want mucking up your inbox. 
This automation auto-archives items that you would like to dissapear after remaining in the inbox for a specific period of time. 

The installation of this script creates a spreadsheet with three different columns. 

- **Name:** The name of the label that you would like to use to filter your email
- **Time:** The number of hours that you would like the email to stay in your inbox before it dissapears. This can be as few as 1 hour and as many hours as you like.
- **Remove Label:** TRUE or FALSE. When we're arechiving this label it may be helpful to remove the label from an archived email. This makes sense for generic labels like 24 or 48, but doesn't make sense for others like "newsletters" or "finances"
- **Delete:** TRUE or FALSE. Not everyone likes to keep their emails around forever. If you would like to delete emails with a specific label, set this to TRUE. 

The spreadsheet that we create when installing this automation includes three different time delay labels

- **24 Hours** - This is good for newsletter and other items that aren't important after a day or so
- **48 Hours** - Good for notifications and other information that you may actually want to dig into
- **1 week** - If you haven't dealt with an email after 1 week, you probably won't deal with it